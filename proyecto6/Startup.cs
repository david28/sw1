﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(proyecto6.Startup))]
namespace proyecto6
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
